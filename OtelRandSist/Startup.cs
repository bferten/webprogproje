﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OtelRandSist.Startup))]
namespace OtelRandSist
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
